package simulation;

public class Constants {
	private Constants() {} //コンストラクタのインスタンス生成を防止
	
	//シミュレーション条件
	public static final int MAXNODE = 200; //配置するノード数
	public static final int MAXLOOP = 1000; //ループ数
	
	//シミュレーション条件　デバック用
//	public static final int MAXNODE = 50; //配置するノード数
//	public static final int MAXLOOP = 10; //ループ数
	
	//シミュレーション領域の設定
	public static final int NUMBEROFDIV = 20; //分割数
	public static final double MINVERTEX = -2000;
	public static final double MAXVERTEX =  2000;
	
	//パラメータの設定
	public static final double BETA =   0; //閾値
	public static final double Q    = 1.0; //電力
	public static final double ETA  = 2.0; //2~4に設定
	public static final double K    = 1.0; //
	public static final double NOISE  = 0.000001; //ノイズ
	
	//パラメータから送信領域の設定
	public static final double TRANSMITRANGE = Math.pow((K*Q) / ( Math.pow(10,BETA/10) * NOISE ), 1/ETA);
	public static final double DX = 2*TRANSMITRANGE / NUMBEROFDIV; //微小距離
	
	//送信ノードの座標
	public static final double Xs =   0;   //送信ノード x座標
	public static final double Ys =   0;   //送信ノード y座標
	
	//受信ノードの座標
	public static final double Xr = 400;   //受信ノード x座標
	public static final double Yr =   0;   //受信ノード y座標
	
	//保存するメソッド
	public static void save() {
		analysis.CSVprintWriter pw = new analysis.CSVprintWriter("data/CONSTANTS.csv");
		pw.println("Maximum number of nodes:","MAXNODE",MAXNODE);
		pw.println("Maximum nubmer of loops:","MAXLOOP",MAXLOOP);
		pw.println("Maximum nubmer of division:","NUMBEROFDIV",NUMBEROFDIV);
		pw.println("Maximum nubmer of division:","MAXVERTEX",MAXVERTEX);
		pw.println("Threshold:","BETA",BETA);
		pw.println("Signal strength:","Q",Q);
		pw.println("ETA:","ETA",ETA);
		pw.println("K:","K",K);
		pw.println("Noise strength:","NOISE",NOISE);
		pw.println("Maximum transmit range","TRANSMITRANGE",TRANSMITRANGE);
		pw.println("Minute distance (dx)","DX",DX);
		pw.println("Coordinate of sender node","(Xs:Ys)","("+Xs+":"+Ys+")");
		pw.println("Coordinate of receiver node","(Xr:Yr)","("+Xr+":"+Yr+")");
		pw.close();
	}
}
