package simulation;
import static simulation.Constants.*;
import java.util.*;

public class Hop2method {
	//フィールド宣言
	private int node;
	private int loop;
	
	public double s_receiver;
	public int enable;
	public int disable;
	public int number_of_jammer=0;
	public int under_BETA = 0;
	
	//コンストラクタ
	public Hop2method(int node, int loop) {
		this.node = node;
		this.loop = loop;
	}
	
	//runメソッド
	public double run() {
		
		//変数宣言
		double Xneighbor,Yneighbor;
		double ttl_p = 0;
		double s_area;
		List<Integer> combination = new ArrayList<>();
		List<Integer> hop_1 = new ArrayList<>();
		List<Integer> hop_2 = new ArrayList<>();
		double coverage;
		
		//乱数クラスのインスタンス化
		int seed = this.node*1000 + this.loop;
		Random random = new Random(seed);
		
		//送信ノードの生成
		Node sender = new Node(Xs,Ys);
//		System.out.println(sender.d_sender);
		
		//近隣ノードの生成
		Node[] neighbor = new Node[node];
		for (int i=0; i<this.node; i++) {
			Xneighbor = random.nextDouble()*(MAXVERTEX-MINVERTEX) + MINVERTEX;  //x座標
			Yneighbor = random.nextDouble()*(MAXVERTEX-MINVERTEX) + MINVERTEX;  //y座標
//			System.out.println("(" +Xneighbor + ", " + Yneighbor + ")");
			neighbor[i] = new Node(Xneighbor, Yneighbor);
		}
		
		/////////// シミュレーション判定 ///////////
		
		//どの近隣ノードがノイズ無線信号送信ノードになるか判定
		//1ホップ近隣ノード
		for (int i=0; i<this.node; i++) {
			if (neighbor[i].d_sender <= TRANSMITRANGE && TRANSMITRANGE < neighbor[i].d_receiver) {
				hop_1.add(i); //hop_1リストに1ホップ近隣ノードを保存
			}
		}
		//2ホップ近隣ノード
		double d;
		for (int value : hop_1) {
			for (int j=0; j<this.node; j++) {
				d = distance(neighbor[value].Xnode, neighbor[value].Ynode, neighbor[j].Xnode, neighbor[j].Ynode);
				//2hop近隣ノードにするための条件分岐
				if (d != 0 && d <= TRANSMITRANGE && TRANSMITRANGE < neighbor[j].d_receiver && hop_1.contains(j) == false && hop_2.contains(j) == false) {
					hop_2.add(j); //hop_2リストに2ホップ近隣ノードを保存
				}
			}
		}
		
		//combinationリストにどのノードがノイズ無線信号送信ノードになるか保存 採用された近隣ノードの番号をcombinationに保存
//		combination.addAll(hop_1);
		combination.addAll(hop_2);
		this.number_of_jammer = combination.size();
		
//		System.out.println();
//		System.out.println("hop_1 :"+hop_1);
//		System.out.println("hop_2 :"+hop_2);
//		System.out.println(combination);
//		System.out.println(combination.size());
		
		//受信ノードに与える受信電波強度の計算 採用された近隣ノードから受信ノードに影響を与える受信電波強度 (ttl_p) を計算
		for(int value : combination) {
//			System.out.println(value);
			ttl_p += neighbor[value].p_receiver; //ノイズ無線信号送信ノードからトータル受信電波強度
		}
//		System.out.println("ttl_p = " + ttl_p);
//		s_receiver = sn(sender.p_receiver, ttl_p); //受信ノードの座標でのSNの計算
		this.s_receiver = sn(sender.p_receiver, ttl_p); //受信ノードの座標でのSNの計算
		
		//各座標の受信電波強度の計算から、カバレッジを計算する
		this.enable = 0;
		this.disable = 0;
		for ( int x=0; x<NUMBEROFDIV; x++ ) {
			for ( int y=0; y<NUMBEROFDIV; y++ ) {
				
				ttl_p = 0; //ttl_pの初期化
				for(int value : combination) {
//					System.out.println(value);
					ttl_p += neighbor[value].p_area[x][y]; //ノイズ無線信号送信ノードからトータル受信電波強度
				}
				s_area = sn(sender.p_area[x][y], ttl_p);
				
				//受信できる座標とできない座標の数を数える
				if (sender.d_area[x][y] <= TRANSMITRANGE) {
					this.enable += 1;
					if ( s_area < BETA ) {
						this.disable += 1;
					}
				}
			}
		}
		
		coverage = coverage(enable, disable); //カバレッジの計算
		if ( this.s_receiver < BETA ) {
			coverage = 0;
			under_BETA=1;
//			System.out.print("<BETA ");
		}
		return coverage;
	}
	
	////////////メソッド////////////
	
	//S/Nの計算
	private static double sn(double ps, double ttlP) {
		return Math.log10( ps / ( NOISE + ttlP ) );
	}
	//カバレッジの計算
	public static double coverage(int all, int part) {
	    if (all == 0 || part == 0) {
	    		return 0;
    		}else {
    			return (double)part/all;
	    }
    }
	//距離の計算
	private static double distance(double x1, double y1, double x2, double y2) {
		double d = Math.sqrt( Math.pow(x2 - x1,2) + Math.pow(y2 - y1,2) );
		return d;
	}
}
