package simulation;
import static simulation.Constants.*;

public class Main {
	//メイン関数
	public static void main(String[] args) {
		
		Main sim = new Main(); //Mainクラスのインスタンス化
		save();                //Constantsクラスに定義した定数をcsvファイルに保存するメソッドの実行
		sim.simulation();      //シミュレーション実行
		analysis.Analysis.processing(); //出力したデータを分析処理
	}
	
	//シミュレーションメソッド
	private void simulation() {
		double coverage;
		
		//ノード数のループと平均のための2重ループ
		//引数にノード数とループ数を指定
		for (int i=0; i<=MAXNODE; i++) {
			System.out.println();
			System.out.println("ノード数："+ i);
			System.out.println("ループ数");
			
			//ファイル出力クラスインスタンス化
			String fileName = String.format("data/NODE_COUNT_%03d.csv", i);
			analysis.CSVprintWriter pw = new analysis.CSVprintWriter(fileName);
			//////////ファイル出力　上部に項目を指定//////////
			pw.print("No.");
			pw.print("Coverage");
			pw.print("Number of jammer nodes");
			pw.print("SN >= beta");
			pw.print("SN < beta");
			pw.print("SN of receiver");
			pw.print("Number of nodes which is under BETA");
			pw.println();
			//////////ファイル出力　上部に項目を指定//////////
			
			for (int j=1; j<=MAXLOOP; j++) {
				System.out.print(j + " ");
				
				Hop2method method = new Hop2method(i,j);
				coverage = method.run();
				
				//////////ファイル出力//////////
				pw.print(j);
				pw.print(coverage);
				pw.print(method.number_of_jammer);
				pw.print(method.enable);
				pw.print(method.disable);
				pw.print(method.s_receiver);
				pw.print(method.under_BETA);
				pw.println();
				//////////ファイル出力//////////
			}
			pw.close(); //ファイルを閉じる
		}
	}
}
